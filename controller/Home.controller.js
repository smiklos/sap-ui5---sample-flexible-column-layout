sap.ui.define([
	'sap/ui/core/mvc/Controller',
	'sap/ui/model/json/JSONModel',
	'sap/m/MessageBox',
	'sap/f/FlexibleColumnLayoutSemanticHelper',
	'sap/m/MessageToast'
], function (Controller, JSONModel, MessageBox, FlexibleColumnLayoutSemanticHelper, MessageToast) {
	"use strict";
	var selectedTestReportURI = "";

	return Controller.extend("sample-namespace.controller.Home", {
		onInit: function () {
			var json = {
				masterItems: [
					{
						"Name" : "Name 1",
						"Detail": "Description 1",
						"Report" : {
							"status": "success",
							"details" : [{
								"id" : "91a62b3f-f4b4-4cf2-8bce-d635461b711a",
								"info": "Report info 1-1",
								"status" : "success" 
							},{
								"id" : "91a62b3f-f4b4-4cf2-8bce-d635461b711b",
								"info": "Report info 1-2",
								"status" : "success" 
							},{
								"id" : "91a62b3f-f4b4-4cf2-8bce-d635461b711c",
								"info": "Report info 1-3",
								"status" : "success" 
							}]
						}
					},{
						"Name" : "Name 2",
						"Detail": "Description 2",
						"Report" : {
							"status": "error",
							"details" : [{
								"id" : "94b23925-2702-4200-8149-7685ac86e207",
								"info": "Report info 2-1",
								"status" : "success" 
							},
							{
								"id" : "94b23925-2702-4200-8149-7685ac86e208",
								"info": "Report info 2-2",
								"status" : "error" 
							}]
						}
					}

				]
			};
			var layoutMmodel = new JSONModel({ layout: "TwoColumnsMidExpanded" });
			var consoleModel = new JSONModel({ console: "TwoColumnsMidExpanded" });
			var mainModel = new JSONModel(json);
			this.getView().setModel(mainModel);
			this.getView().setModel(layoutMmodel, "layout");
			this.getView().setModel(consoleModel, "console");
			//this.initPage();
		},

		onMasterItemPressed: function (evt) {
			console.log("onMasterItemPressed");
			var data = evt.getSource().getBindingContext().getObject();
			console.log(data);
			this.getView().getModel().setProperty("/selectedMasterItem", data);
			//set detail model
			this.getView().byId("actionButton").setVisible(true);

		},
		onMasterButtonPressed: function (evt) {
			console.log("onMasterButtonPressed");
			$.post('/runtest', { "email": encrypt_email, "pass": encrypt_pass }, (data) => {
				this.refresMasterItems();
				this.showNotification("success", "post sucessfull: " + data);
			}).fail((error) => {
				console.error(error.responseText);
				this.showNotification("error", "post failed : " + error.responseText);
				this.refresMasterItems();
			});
		},
		getCookie: function (name) {
			var value = "; " + document.cookie;
			var parts = value.split("; " + name + "=");
			if (parts.length == 2) return parts.pop().split(";").shift();
		},
		initPage: function () {
			//this.initConnectionToBackEnd();
			this.refresMasterItems();
		},
		initConnectionToBackEnd: function () {
			$.get('/initConnection', (data) => {
				this._publicKey = atob(data);
				//this.showNotification("success", "Connection Initialized to back-end");
				this.writeToConsole("Connection Initialized to back-end");
			});
		},
		refresMasterItems: function () {
			this.getView().byId("masterItems").setBusy(true);
			$.get('/getMasterItems', (data) => {
				this.getView().getModel().setData(data);
				this.getView().byId("masterItems").setBusy(false);
			}).fail((error) => {
				console.error(error.responseText);
				this.showNotification("error", "Cannot get master items: " + error.responseText);
				this.getView().byId("masterItems").setBusy(false);
			});
		},
		/**
		 * @param type : string = success | warning | error
		 * @param message : string
		 */
		showNotification: function (type, message) {
			var bCompact = !!this.getView().$().closest(".sapUiSizeCompact").length;
			MessageBox[type](
				message,
				{
					styleClass: bCompact ? "sapUiSizeCompact" : ""
				}
			);
		},
		setLayout: function (layout) {
			this.getView().getModel("layout").setProperty("/layout", layout);

		},
		layoutHelper: function (num) {
			var oFCL = this.getView().byId("fcl");
			var oParams = jQuery.sap.getUriParameters(),
				oSettings = {
					defaultTwoColumnLayoutType: sap.f.LayoutType.TwoColumnsMidExpanded,
					defaultThreeColumnLayoutType: sap.f.LayoutType.ThreeColumnsEndExpanded,
				};
			var nextState = FlexibleColumnLayoutSemanticHelper.getInstanceFor(oFCL, oSettings).getNextUIState(num);
			this.setLayout(nextState.layout);
		},
		setFullScreenOnDetails: function () {
			
		},
		infoStateFormatter: function (sStatus) {
			console.log("infoStateFormatter",sStatus);
			if (sStatus === "success") {
				return "Success";
			} else if (sStatus === "error") {
				return "Error";
			} else {
				return "Warning";
			}
		}

	});
});